﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraApp1
{
    public class User
    {
        public int user_id { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }

        public User(int User_id, string Lastname, string Firstname)
        {
            user_id = User_id;
            lastname = Lastname;
            firstname = Firstname;

        }

        public override string ToString()
        {
            return user_id.ToString()+" "+lastname+" "+firstname;
        }

    }
}
