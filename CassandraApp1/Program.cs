﻿using Cassandra;
using Cassandra.Mapping;
using CassandraApp1;

Cluster cluster=Cluster.Builder().AddContactPoint("cassandra").Build();
ISession session=cluster.Connect();

session.Execute("DROP KEYSPACE IF EXISTS userprofile");
session.Execute("Create KeySpace UserProfile WITH REPLICATION= { 'class' : 'NetworkTopologyStrategy', 'datacenter1' : 1   } ; ");
Console.WriteLine("Created keyspace userprofile");
session.Execute("CREATE TABLE IF NOT EXISTS userprofile.user ( user_id int PRIMARY KEY,  lastname text,  firstname text );");
Console.WriteLine("Table was created");
session= cluster.Connect("userprofile");

IMapper mapper=new Mapper(session);
mapper.Insert<User>(new User(1, "Ivanov", "Ivan"));
mapper.Insert<User>(new User(2, "Petrov", "Ivan"));
mapper.Insert<User>(new User(3, "Sidorov", "Ivan"));

Console.WriteLine("Data has inserted");

foreach(User u in mapper.Fetch<User>("Select * from user"))
{
    Console.WriteLine(u.ToString());
}



